package tests;

import junit.framework.TestCase;

public class TestExample extends TestCase {
	public void testExample() {
		int result = 5 - 2;
		assertEquals("Attended: 3", result, 3);
		assertTrue("Attended: true", true);
		assertNull("Attended: NULL", null);
		assertNotSame("Attended: true", 3, 4);
		assertNotNull("Attended: Not NULL", 3);
	}
}
